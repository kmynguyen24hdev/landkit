/* Show hide navbar */

var navbar = document.getElementById('collapse')

function showNavbar() {
  navbar.classList.remove('hide-on-tablet-mobile')
}

function hideNavbar() {
  navbar.classList.add('hide-on-tablet-mobile')

} 

function showHideMiniMenu() {
  var menuItems = document.querySelectorAll(".subnav")

  console.log(menuItems)
  for(var i = 0 ; i < menuItems.length; i++){
    var menuItem = menuItems[i]
    menuItem.onclick = function() {
      event.preventDefault()
      console.log(this)
      
      //icon up down
      var upIcon = this.querySelector('.list-group-item--icon .fa-angle-up')
      var downIcon = this.querySelector('.list-group-item--icon .fa-angle-down')
      
      
      console.log(this.nextElementSibling)
      let menuItem = this.nextElementSibling;
      
      // open close menu mini
      if (!menuItem.style.display || menuItem.style.display == 'none') {
        menuItem.style.display = 'block ';
        downIcon.classList.add("hide-on-tablet-mobile"); 
        upIcon.classList.remove("hide-on-tablet-mobile"); 
      } else {
        menuItem.style.display = 'none';
        downIcon.classList.remove("hide-on-tablet-mobile"); 
        upIcon.classList.add("hide-on-tablet-mobile"); 
      }
    }
  }
}

showHideMiniMenu();


/* Slider */
let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  let i;
  let slides = document.getElementsByClassName("slider-item");
  let imgSlides = document.getElementsByClassName("slider-img");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }

  slides[slideIndex-1].style.display = "flex";  

}

/* Scroll animation */
function reveal() {
  var reveals = document.querySelectorAll(".reveal");


  for (var i = 2; i < reveals.length; i++) {
    var windowHeight = window.innerHeight;
    var elementTop = reveals[i].getBoundingClientRect().top; // vị trí của reveals thứ i so với top
    
    if (elementTop <= windowHeight ) {
      reveals[i].classList.add("active");
    } 
  }
}
  
window.addEventListener("scroll", reveal);


// get input value
function getValue(id){
  return document.getElementById(id).value.trim();
}

// show input error
function showError(key, mess){
  let idError = document.getElementById(key + '_error')
  idError.innerHTML = mess;
  idError.classList.add('pt-3')
}

//handle Form Validation
function handleFormValidation() {
  event.preventDefault();
  // Name user 
  var username = getValue('username');
  if(username == '' ) {
 
    showError('username', 'Please enter username')
  } else {
    showError('username', '')
    console.log(username)
  }

  //email input
  var email = getValue('email')
  var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
  if(email == '') {

    showError('email', 'Please enter a email')
  } else if (!mailformat.test(email)) {

    showError('email', 'Email format not valid') 
  } else {
    showError('email', '')
    console.log(email);
  }

  //password

  var password = getValue('password')
  if(password == '') {

    showError('password', 'Please enter password')
  } else if (password.length < 8) {

    showError('password', 'Password length must be greater 8')
  } else {
    showError('password', '')
    console.log(password)
  }
}