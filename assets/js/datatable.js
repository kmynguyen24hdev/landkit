var dataSet = [
  [
    "Tiger Nixon",
    "System Architect",
    "Edinburgh",
    "5421",
    "2011/04/25",
    "$320,800",
  ],
  ["Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750"],
  [
    "Ashton Cox",
    "Junior Technical Author",
    "San Francisco",
    "1562",
    "2009/01/12",
    "$86,000",
  ],
  [
    "Cedric Kelly",
    "Senior Javascript Developer",
    "Edinburgh",
    "6224",
    "2012/03/29",
    "$433,060",
  ],
  ["Airi Satou", "Accountant", "Tokyo", "5407", "2008/11/28", "$162,700"],
  [
    "Brielle Williamson",
    "Integration Specialist",
    "New York",
    "4804",
    "2012/12/02",
    "$372,000",
  ],
  [
    "Herrod Chandler",
    "Sales Assistant",
    "San Francisco",
    "9608",
    "2012/08/06",
    "$137,500",
  ],
  [
    "Rhona Davidson",
    "Integration Specialist",
    "Tokyo",
    "6200",
    "2010/10/14",
    "$327,900",
  ],
  [
    "Colleen Hurst",
    "Javascript Developer",
    "San Francisco",
    "2360",
    "2009/09/15",
    "$205,500",
  ],
  [
    "Sonya Frost",
    "Software Engineer",
    "Edinburgh",
    "1667",
    "2008/12/13",
    "$103,600",
  ],
  ["Jena Gaines", "Office Manager", "London", "3814", "2008/12/19", "$90,560"],
  [
    "Quinn Flynn",
    "Support Lead",
    "Edinburgh",
    "9497",
    "2013/03/03",
    "$342,000",
  ],
  [
    "Charde Marshall",
    "Regional Director",
    "San Francisco",
    "6741",
    "2008/10/16",
    "$470,600",
  ],
  [
    "Haley Kennedy",
    "Senior Marketing Designer",
    "London",
    "3597",
    "2012/12/18",
    "$313,500",
  ],
  [
    "Tatyana Fitzpatrick",
    "Regional Director",
    "London",
    "1965",
    "2010/03/17",
    "$385,750",
  ],
  [
    "Michael Silva",
    "Marketing Designer",
    "London",
    "1581",
    "2012/11/27",
    "$198,500",
  ],
  [
    "Paul Byrd",
    "Chief Financial Officer (CFO)",
    "New York",
    "3059",
    "2010/06/09",
    "$725,000",
  ],
  [
    "Gloria Little",
    "Systems Administrator",
    "New York",
    "1721",
    "2009/04/10",
    "$237,500",
  ],
  [
    "Bradley Greer",
    "Software Engineer",
    "London",
    "2558",
    "2012/10/13",
    "$132,000",
  ],
  ["Dai Rios", "Personnel Lead", "Edinburgh", "2290", "2012/09/26", "$217,500"],
  [
    "Jenette Caldwell",
    "Development Lead",
    "New York",
    "1937",
    "2011/09/03",
    "$345,000",
  ],
  [
    "Yuri Berry",
    "Chief Marketing Officer (CMO)",
    "New York",
    "6154",
    "2009/06/25",
    "$675,000",
  ],
  [
    "Caesar Vance",
    "Pre-Sales Support",
    "New York",
    "8330",
    "2011/12/12",
    "$106,450",
  ],
  [
    "Doris Wilder",
    "Sales Assistant",
    "Sydney",
    "3023",
    "2010/09/20",
    "$85,600",
  ],
  [
    "Angelica Ramos",
    "Chief Executive Officer (CEO)",
    "London",
    "5797",
    "2009/10/09",
    "$1,200,000",
  ],
  ["Gavin Joyce", "Developer", "Edinburgh", "8822", "2010/12/22", "$92,575"],
  [
    "Jennifer Chang",
    "Regional Director",
    "Singapore",
    "9239",
    "2010/11/14",
    "$357,650",
  ],
  [
    "Brenden Wagner",
    "Software Engineer",
    "San Francisco",
    "1314",
    "2011/06/07",
    "$206,850",
  ],
  [
    "Fiona Green",
    "Chief Operating Officer (COO)",
    "San Francisco",
    "2947",
    "2010/03/11",
    "$850,000",
  ],
  [
    "Shou Itou",
    "Regional Marketing",
    "Tokyo",
    "8899",
    "2011/08/14",
    "$163,000",
  ],
  [
    "Michelle House",
    "Integration Specialist",
    "Sydney",
    "2769",
    "2011/06/02",
    "$95,400",
  ],
  ["Suki Burks", "Developer", "London", "6832", "2009/10/22", "$114,500"],
  [
    "Prescott Bartlett",
    "Technical Author",
    "London",
    "3606",
    "2011/05/07",
    "$145,000",
  ],
  [
    "Gavin Cortez",
    "Team Leader",
    "San Francisco",
    "2860",
    "2008/10/26",
    "$235,500",
  ],
  [
    "Martena Mccray",
    "Post-Sales support",
    "Edinburgh",
    "8240",
    "2011/03/09",
    "$324,050",
  ],
  [
    "Unity Butler",
    "Marketing Designer",
    "San Francisco",
    "5384",
    "2009/12/09",
    "$85,675",
  ],
];

var tabletContent = document.getElementById("dataBody");
var search = document.getElementById("searchInput");
var lengthLimit = document.getElementById("select-length");
var entryDesc = document.getElementById("show-number");
var pageNumber = document.getElementById("page-number");
var lengthDataSet = dataSet.length;
var currentPage = 1;
var maxPage = Math.ceil(lengthDataSet / lengthLimit.value);
var dataCurrent = [...dataSet];
var start = 0;
var end = 0;
var limit = 10

lengthLimit.addEventListener("change", (e) => {
  e.preventDefault();
  if (currentPage > 1) {
    currentPage = 1;
  }
  limit = e.target.value;
  maxPage = Math.ceil(lengthDataSet / limit);
  render(dataCurrent, limit, currentPage);
});

search.addEventListener("keyup", (e) => {
  e.preventDefault();
  let textInput = e.target.value;
    if(textInput != ''){
      let result = dataSet.filter(element => {
          let flat = false;
          element.map(item => {
              if (item.toLowerCase().includes(textInput.toLowerCase())) {
                  flat = true
              }
          })
          return flat
      })
        dataCurrent = [...result]
        lengthDataSet = dataCurrent.length
        maxPage = Math.ceil(lengthDataSet/ limit)
        console.log(maxPage)
        render(dataCurrent, limit, 1); 
        if(result.length <=0) {
          entryDescription(-1, 0, lengthDataSet)
        }
    }else{
      render(dataSet, limit, 1);
    }
});


function entryDescription(start, end, total) {
  entryDesc.innerHTML = `Show ${
    start + 1
  } to ${end} entries of ${total} entries`;
}

function btnNext() {
  if (currentPage == maxPage) {
    return;
  }
  currentPage++;
  render(dataCurrent, lengthLimit.value, currentPage);
}

function btnPre() {
  if (currentPage === 1) {
    return;
  }
  currentPage--;
  render(dataCurrent, lengthLimit.value, currentPage);
}

function showPaginations() {
  let btnPage = "";
  for (var i = 1; i <= maxPage; i++) {
    btnPage += `<button class="p-2" onclick='onPage(${i})'>${i}</button>`;
  }
  pageNumber.innerHTML = btnPage;
}

function onPage(i) {
  currentPage = i;
  render(dataCurrent, lengthLimit.value, currentPage);
}

var switching = [false, false, false, false, false, false];
function sortTable(n) {
  let btnDown = document.querySelectorAll("#btn-down");
  let btnUp = document.querySelectorAll("#btn-up");

  for (i = 0; i < btnDown.length; i++) {
    btnDown[i].classList.remove("active");
    btnUp[i].classList.remove("active");
  }

  dataCurrent.sort(function (a, b) {
    if (switching[n]) {
      let temp = a;
      a = b;
      b = temp;
      btnUp[n].classList.add("active");
    } else {
      btnDown[n].classList.add("active");
    }
    if (n < 3) {
      const nameA = a[n].toUpperCase();
      const nameB = b[n].toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }

      return 0;
    } else if (n == 3) {
      return a[n] - b[n];
    } else if (n == 4) {
      let date1 = Date.parse(a[n]);
      let date2 = Date.parse(b[n]);
      return date1 - date2;
    } else if (n == 5) {
      let numberA = a[n].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "");
      let numberB = b[n].replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, "");
      return numberA - numberB;
    }
  });
  switching[n] = switching[n] ? false : true;
  render(dataCurrent, lengthLimit.value, currentPage);
}

function render(data, limit, page) {
  var dataShow = [];

  start = limit * (page - 1);
  end = limit * page < dataCurrent.length ? limit * page : dataCurrent.length;
  for (let i = start; i < end; i++) {
    dataShow.push(data[i]);
  }
  bodyTable = "";
  dataShow.map((dataRow) => {
    let row = "";
    dataRow.map((dataCol) => {
      row += `<td>${dataCol}</td>`;
    });
    bodyTable += `<tr>${row}</tr>`;
  });
  tabletContent.innerHTML = bodyTable;
  showPaginations();
  entryDescription(start, end, dataCurrent.length);
}
sortTable(0)

